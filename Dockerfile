
ARG unit_version='<unit_version>'
ARG alpine_version='<alpine_version>'

FROM alpine:$alpine_version as BUILDER

ARG unit_version

RUN set -ex \
    && apk add --no-cache --update \
    openssl-dev curl gcc musl-dev make pcre2-dev linux-headers \
    && NCPU="$(getconf _NPROCESSORS_ONLN)" \
    && export UNITTMP=$(mktemp -d -p /tmp -t unit.XXXXXX) \
    && cd $UNITTMP \
    && curl -O "https://unit.nginx.org/download/unit-$unit_version.tar.gz" \
    && tar xzf unit-$unit_version.tar.gz \
    && cd unit-$unit_version \
    && echo '*self_spec:' > /tmp/no-pie-compile.specs \
    && echo '+ %{!r:%{!fpie:%{!fPIE:%{!fpic:%{!fPIC:%{!fno-pic:-fno-PIE}}}}}}' >> /tmp/no-pie-compile.specs \
    && ./configure --modules=/usr/lib/unit/modules --control="unix:/var/run/control.unit.sock" \
        --openssl --state=/var/lib/unit --pid=/var/run/unit.pid \
        --log=/dev/stdout --tmp=/var/tmp --user=unit --group=unit \
        --cc-opt='-g -O2 -flto=auto -ffat-lto-objects -specs=/tmp/no-pie-compile.specs -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' \
    && make -j $NCPU unitd \
    && install -pm755 build/unitd /usr/sbin/unitd \
    && mkdir -p /usr/lib/unit/modules/

FROM alpine:$alpine_version

COPY docker-entrypoint.sh /usr/local/bin/
COPY --from=BUILDER /usr/lib/unit/ /usr/lib/unit/
COPY --from=BUILDER /usr/sbin/unitd /usr/sbin/unitd

RUN set -ex \
    && mkdir -p /var/lib/unit/ \
    && mkdir /docker-entrypoint.d/ \
    && apk add --no-cache --update \
        tini ca-certificates curl openssl musl pcre2 \
    && addgroup --system unit \
    && adduser \
      --system \
      --disabled-password \
      --ingroup unit \
      --no-create-home \
      --home /nonexistent \
      --gecos "unit user" \
      --shell /bin/false \
      unit \
    && (addgroup --system www-data || true) \
    && adduser \
      --system \
      --disabled-password \
      --ingroup www-data \
      --no-create-home \
      --home /nonexistent \
      --gecos "www-data user" \
      --shell /bin/false \
      www-data

RUN ldconfig /

STOPSIGNAL SIGTERM

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/docker-entrypoint.sh"]

CMD ["/usr/sbin/unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock"]
