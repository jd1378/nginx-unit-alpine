# alpine nginx unit

an alpine nginx unit for serving static files purpose

## building locally

```sh
docker build --tag nginx-unit-alpine .
```

## Notes

`www-data` user is created and added to the group with the same name for you.

## How to use

1. put your config in a file like `.unit.config.json`
2. copy `.unit.config.json` to `/docker-entrypoint.d/` directory

Example:

```Dockerfile

FROM javadmnjd/nginx-unit-alpine

COPY . /var/www/

RUN chown -R www-data:www-data /var/www \
    && mv /var/www/unit.config.json /docker-entrypoint.d/

WORKDIR /var/www/

CMD ["/usr/sbin/unitd", "--no-daemon", "--control","unix:/var/run/control.unit.sock"]

EXPOSE 80

```

Example `.unit.config.json` for the above Dockerfile:

```json
{
  "listeners": {
    "*:80": {
      "pass": "routes"
    }
  },
  "routes": [
    {
      "match": {
        "uri": [
          "*.manifest",
          "*.appcache",
          "*.html",
          "*.json",
          "*.rss",
          "*.atom",
          "*.jpg",
          "*.jpeg",
          "*.gif",
          "*.png",
          "*.ico",
          "*.cur",
          "*.gz",
          "*.svg",
          "*.svgz",
          "*.mp4",
          "*.ogg",
          "*.ogv",
          "*.webm",
          "*.htc",
          "*.css",
          "*.js",
          "*.ttf",
          "*.ttc",
          "*.otf",
          "*.eot",
          "*.woff",
          "*.woff2",
          "*.txt"
        ]
      },
      "action": {
        "share": "/var/www/$uri",
        "index": "index.html",
        "chroot": "/var/www/",
        "follow_symlinks": false,
        "traverse_mounts": false
      }
    }
  ],
  "access_log": "/dev/stdout"
}
```
